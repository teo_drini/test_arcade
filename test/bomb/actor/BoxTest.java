package bomb.actor;

//import bomb.actor.*;

import java.awt.Graphics2D;
import java.awt.Image;
import javax.swing.ImageIcon;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class BoxTest {
    Box box;
    Image img;
    
    public BoxTest() {
    }
    
    @Before
    public void setUpClass() {
        int x =90;
        int y=405;
        int type = 1;
        String images ="/Images/box1.png";
        img= new ImageIcon(BoxTest.class.getResource(images)).getImage();
        box = new Box(x,y,type, images);
    }
    
    @AfterClass
    public static void tearDownClass() {
    }


    @Test
    public void testGetType() {
        System.out.println("getType");
        int expResult = 1;
        int result = box.getType();
        assertEquals(expResult, result);
    }

    @Test
    public void testIsImpactBoxvsActor() {
        System.out.println("isImpactBoxvsActor");
        Actor actor = new Actor();
        int expResult = 0;
        int result = box.isImpactBoxvsActor(actor);
        assertEquals(expResult, result);
    }

    @Test
    public void testGetX() {
        System.out.println("getX");
        int expResult = 90;
        int result = box.getX();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetY() {
        System.out.println("getY");
        int expResult = 405;
        int result = box.getY();
        assertEquals(expResult, result);

    }

    @Test
    public void testGetWidth() {
        System.out.println("getWidth");
        int expResult = img.getWidth(null);
        int result = box.getWidth();
        assertEquals(expResult, result);
    }

    @Test
    public void testGetHeight() {
        System.out.println("getHeight");
        int expResult = img.getHeight(null);
        int result = box.getHeight();
        assertEquals(expResult, result);
    }
    
}
