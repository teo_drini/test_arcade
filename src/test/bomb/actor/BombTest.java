/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test.bomb.actor;

import bomb.actor.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author orxan
 */
public class BombTest {

    Bomb bomb = new Bomb(5, 5, 5, 5);

    public BombTest() {
    }

    public void setUp() {

    }

    @Test
    public void testGetTimeline() {
        System.out.println("getTimeline");

        int expResult = 5;
        int result = bomb.getTimeline();
        assertEquals(expResult, result);

    }

    @Test
    public void testGetSize() {
        System.out.println("getSize");
        int expResult = 5;
        int result = bomb.getSize();
        assertEquals(expResult, result);

    }

    @Test
    public void testSetRun() {
        System.out.println("setRun");
        Bomber bomber = new Bomber(5, 5, 5, 5, 5, 5, 5);
        boolean expResult = false;
        boolean result = bomb.setRun(bomber);
        assertTrue(result);

    }

    @Test
    public void testSetTimeline() {
        System.out.println("setTimeline");
        int timeline = 7;
        bomb.setTimeline(timeline);

    }

    @Test
    public void testIsImpact() {
        System.out.println("isImpact");
        int xNewBomb = 10;
        int yNewBomb = 10;
        boolean expResult = false;
        boolean result = bomb.isImpact(xNewBomb, yNewBomb);
        assertTrue(result);

    }

    @Test
    public void testIsImpactBombvsActor() {
        System.out.println("isImpactBombvsActor");
        Actor actor = new Actor();
        int result = bomb.isImpactBombvsActor(actor);
        assertEquals(actor.getRunBomb(), result);

    }

}
